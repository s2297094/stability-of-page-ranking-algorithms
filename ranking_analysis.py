import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import random
import statistics
import json
import time
import math
from multiprocessing import Pool

NUM_THREADS = 10
RETAIN_EDGE_DENSITY = True

PATH = "./results/retain_edge_density" if RETAIN_EDGE_DENSITY else "./results/not_retain_edge_density"

# each node n can have n edges, if you want an edge density of d you will have d*n^2 edges


def numEdges(n, dens):
    return int((n*n)*dens)


# create a random graph with the given size and connections
def random_graph_edges(size, connections):
    # set to keep track of edges
    edges = set()
    # adjacency matrix for the size*size graph
    G = np.zeros((size, size))
    # while there are not enough edges yet
    while len(edges) < connections:
        # add random edge
        a = random.randint(0, size-1)
        b = random.randint(0, size-1)
        edges.add((a, b))
        G[a][b] = 1
    return G, edges


# apply list of changes to the graph
def change_graph(G, changes):
    for (a, b) in changes:
        # if there is an edge from a to b, then remove it, otherwise add it
        G[a][b] = 1 - G[a][b]
    return G


# calculate indegree of G
def indegree(G):
    score = np.sum(G, axis=0)
    return (score)


# calculates pagerank of G with 15 itterations and spider-trap avoidance β = 0.85
def pagerank(G):
    k = 15
    b = 0.85
    size = len(G)
    # Get adjacency array
    M = np.copy(G)
    for i in range(size):
        s = sum(M[i, :])
        if s == 0:
            # make dead ends point to themselves
            M[i, i] = 1
        else:
            # normalize row
            M[i, :] /= s
    M = M.transpose()
    initial = np.ones(size)/size
    v = initial.copy()
    # itteratively execute formula
    for _ in range(k):
        # formula from slides
        v = b*M.dot(v) + (1-b)*initial
    return v


# calculates HITS score of G with 15 itterations
def hits(G):
    k = 15
    M = G
    Mt = M.transpose()
    # formula from the slides
    u = np.ones(len(G))
    v = None
    for _ in range(k):
        v = Mt.dot(u)
        u = M.dot(v)
    return v


# calculate the distance between the vectors A and B
def distance(A, B):
    return sum(np.absolute(np.array(A)-np.array(B)))


# transforms rank vector to an ordinal rank vector
def score_to_index(score):
    return (-score).argsort().argsort() + np.ones(len(score))


# compare the rank vecotrs A and B
def compare(A, B):
    r = dict()
    # normalized difference
    r["percentage"] = 100.0 * distance(A, B) / sum(A)
    # normalized ordinal rank difference
    r["ordinal"] = 100.0 * \
        distance(score_to_index(A), score_to_index(B)) / (len(A)*(len(A)+1)/2)
    # normalized difference of node with the biggest change in rank
    r["biggest"] = 100.0 * \
        max(np.absolute(np.array(A)-np.array(B))) / (sum(A)/len(A))
    return r


# generate a graph with the given size (n) and edges.
# over (iters) iterations change the graph with a total link distance of 1% of the total number of edges in the graph
# return the comparison of the rank data at each itteration and the intial rank data
def do_experiment(n, edges, iters):
    # get a random graph
    G, edge_set = random_graph_edges(n, edges)

    # (1% of edges)
    dist = int(edges/100.0)

    # initial rank vectors
    v_page = pagerank(G)
    v_in = indegree(G)
    v_hits = hits(G)

    # lists to store the comparisons over the itterations
    page_data = []
    in_data = []
    hits_data = []

    change_list = []
    if RETAIN_EDGE_DENSITY:
        # get 0.5% of the edges to remove over the duration of the experiment
        subtractions = set(list(edge_set)[0:dist//2])
        # set to keep track of the additions
        additions = set()
        # while the number of changes is lower than the desired link distance
        while len(subtractions)+len(additions) < dist:
            # get random edges
            a = random.randint(0, n-1)
            b = random.randint(0, n-1)
            # check if the edge is not in the set of edges of the graph
            if (a, b) not in edge_set:
                additions.add((a, b))
        # convert additions to a list
        additions = list(additions)
        # interweave the additions and the subtraction to make sure that the edge density stays the same
        change_list = [x for y in zip(additions, subtractions) for x in y]
        # if there are an uneven number of chances add the last change
        if len(additions) > len(subtractions):
            change_list += [additions[-1]]
    else:
        change_set = set()
        while len(change_set) < dist:
            # get random edges
            a = random.randint(0, n-1)
            b = random.randint(0, n-1)
            # check if the edge is not in the set of changes
            if (a, b) not in change_set:
                change_set.add((a, b))
        change_list = list(change_set)
    # calculate the number of changes for each ittertation
    step_size = (1.0 * dist) / iters
    for i in range(iters):
        # apply the changes to the graph
        G = change_graph(
            G, change_list[int(math.ceil(i*step_size)):int(math.ceil((i+1)*step_size))])
        # store comparison with original graph
        page_data.append(compare(v_page, pagerank(G)))
        in_data.append(compare(v_in, indegree(G)))
        hits_data.append(compare(v_hits, hits(G)))
    # return collected data
    return page_data, in_data, hits_data


# multithreaded part of do_multi_experiment
def multithreaded(args):
    (n, edges, iters, tests, i) = args

    # time the call to do_experiment
    start_time = time.time()
    page_data, in_data, hits_data = do_experiment(n, edges, iters)
    # log the duration of the call to do_experiment
    end_time = time.time()
    print(str(i)+"/"+str(tests)+f" t:{end_time-start_time:.3f}")
    # return result of call to do_experiment
    return page_data, in_data, hits_data


# run do_experiment (tests) times in order to be able to calculate an average
def do_multi_experiment(iters, n, edges, tests):
    # lists to store the results
    page_datas = []
    in_datas = []
    hits_datas = []

    # multithread to speed up calculations
    with Pool(NUM_THREADS) as p:
        # not multithreaded version
        # for (page_data, in_data, hits_data) in map(multithreaded, map(lambda i: (n, edges, iters, tests, i), range(tests))):

        # multithread
        for (page_data, in_data, hits_data) in p.imap_unordered(multithreaded, map(lambda i: (n, edges, iters, tests, i), range(tests)), chunksize=int(math.ceil((tests/NUM_THREADS)))):
            # store results of one call to do_experiment
            page_datas.append(page_data)
            in_datas.append(in_data)
            hits_datas.append(hits_data)
    # generate the datas corresponding xs values (from 0 to 1% of edges)
    xs = 100 * \
        np.array(range(1, len(page_datas[0])+1))/(len(page_datas[0])*100.0)
    # return data from the multiple experiment
    return xs, page_datas, in_datas, hits_datas


# combine a list of lists into a single list by calling f
def combine(f, datas):
    return list(map(f, zip(*datas)))


# get data with a specific property from a list of dictionaries/objects
def get_prop(datas, prop):
    return list(map(lambda data: list(map(lambda x: x[prop], data)), datas))


# calculate standard deviation, mean, mean +- standard deviation, min (low) and max (high) from multiple experiments
def getRanges(datas):
    std = combine(statistics.stdev, datas)
    mean = combine(statistics.mean, datas)
    std_h = list(map(lambda v: v[0]+v[1], zip(mean, std)))
    std_l = list(map(lambda v: v[0]-v[1], zip(mean, std)))
    low = combine(min, datas)
    high = combine(max, datas)
    return low, std_l, mean, std_h, high


# do (multi_experiment_size) number of tests for each of pair of sizes (ns) and number of edges (edges)
# store the resulting data as json in a file
def do_range_test(name, ns, edges, multi_experiment_size, res):
    # list to store results
    results = []
    # for each pair of size and edges given:
    for (n, edg, i) in zip(ns, edges, range(len(ns))):
        # save start time
        start_time = time.time()
        # log which test is running now
        print("\nStarting range test "+str(n)+" "+str(edg)+":")
        # create a unique string describing these tests
        range_test_description = "n_" + \
            str(n)+",edges_"+str(edg)+",multi_experiment_size_" + \
            str(multi_experiment_size)
        # execute a multi experiment with the current n and edges
        xs, page_datas, in_datas, hits_datas = do_multi_experiment(
            res, n, edg, multi_experiment_size)
        # store the results in an object
        results.append({
            "description": range_test_description,
            "n": n,
            "edges": edg,
            "res": res,
            "page": page_datas,
            "hits": hits_datas,
            "in": in_datas,
            "multi_experiment_size": multi_experiment_size,
            "xs": list(xs)
        })
        # if this was not the last itteration save the current progres in a file
        if i != len(ns)-1:
            with open(PATH+'/data/data'+name+'_'+str(i)+'.json', 'w') as f:
                json.dump(results, f)
        # log the time of this itteration
        end_time = time.time()
        print(f"It took {end_time-start_time:.2f} seconds to compute")
    # store the data of all tests in a file
    with open(PATH+'/data/' + name + '.json', 'w') as f:
        json.dump(results, f)


# plot stats given the data needed to plot
def plot_stats(axs, xs, low, std_l, mean, std_h, high):
    axs.fill_between(xs, low, high, color=matplotlib.colors.to_rgba(
        'gray', 0.3), label="Min Max")
    axs.fill_between(xs, std_l, std_h, color=matplotlib.colors.to_rgba(
        'gray', 0.5), label="μ ± σ")
    axs.plot(xs, mean, label="μ")
    axs.legend(loc='upper left')


# create a big plot with the given title
def create_plot(title):
    fig, axs = plt.subplots(3, sharex=False, sharey=False)
    fig.set_size_inches(14, 12)
    fig.suptitle(title,fontweight="bold")
    return fig, axs


# lable the plot and set the layout
def lable_plot(fig, axs):
    fig.supxlabel("Link distance (% of edges)")
    axs[0].legend(loc='upper left')
    axs[0].set_title("Difference depending on link distance")
    axs[0].set_ylabel("Difference (% of sum of the original rank vector)")
    axs[1].legend(loc='upper left')
    axs[1].set_title("Ordinal rank difference depending on link distance")
    axs[1].set_ylabel("Ordinal rank difference (% of sum of ordinal rank vector)")
    axs[2].legend(loc='upper left')
    axs[2].set_title(
        "Biggest node rank difference depending on link distance")
    axs[2].set_ylabel("Node rank difference (% of average node rank)")
    fig.tight_layout()


# generate all plots
def generate_plots():
    print("Generating plots")
    all_pagerank = []
    all_in = []
    all_hits = []
    xs = [x/100 for x in range(100)]
    for (file) in ["data_var_dens", "data_var_size"]:
        with open(PATH+'/data/'+file+'.json', 'r') as f:
            data = json.load(f)
            for test in data:
                all_pagerank.extend(test["page"])
                all_in.extend(test["in"])
                all_hits.extend(test["hits"])
    fig, axs = create_plot(
        "Comparison of the algorithms with the mean results of the tests")
    for (datas, name) in [(all_pagerank, "PageRank"), (all_hits, "HITS"), (all_in, "InDegree")]:
        axs[0].plot(xs, combine(statistics.mean, get_prop(
            datas, "percentage")), label="mean of " + name)
        axs[1].plot(xs, combine(statistics.mean, get_prop(
            datas, "ordinal")), label="mean of " + name)
        axs[2].plot(xs, combine(statistics.mean, get_prop(
            datas, "biggest")), label="mean of " + name)
    lable_plot(fig, axs)
    fig.savefig(PATH+"/out/mean.jpg")
    matplotlib.pyplot.close(fig)
    # for the 2 experiments (variable density and variable size):
    for (file, experiment_type) in [("data_var_dens", "variable graph density"), ("data_var_size", "variable graph size")]:
        # open and load the stored data
        with open(PATH+'/data/'+file+'.json', 'r') as f:
            data = json.load(f)
            # for each (multi) test done:
            for test in data:
                # get data from the json object of this test
                range_test_description = test["description"]
                n = test["n"]
                dens = 1.0*test["edges"]/(n*n)
                xs = test["xs"]
                page_datas = test["page"]
                hits_datas = test["hits"]
                in_datas = test["in"]
                multi_experiment_size = test["multi_experiment_size"]
                # for each algorithm:
                for (datas, name) in [(page_datas, "PageRank"), (hits_datas, "HITS"), (in_datas, "InDegree")]:
                    # create a plot
                    fig, axs = plt.subplots(3, sharex=False, sharey=False)
                    fig.set_size_inches(10, 10)
                    fig.suptitle(name+" with n="+str(n)+f", link density={dens:.2f}" +
                                 " and randomized tests=" + str(multi_experiment_size))
                    fig.supxlabel("Link distance (% of edges)")

                    # plot the 3 measured statistics
                    plot_stats(
                        axs[0], xs, *getRanges(get_prop(datas, "percentage")))
                    plot_stats(
                        axs[1], xs, *getRanges(get_prop(datas, "ordinal")))
                    plot_stats(
                        axs[2], xs, *getRanges(get_prop(datas, "biggest")))
                    lable_plot(fig, axs)
                    # save and close the plot
                    fig.savefig(PATH+"/out/variance/"+name+"_" +
                                range_test_description+".jpg")
                    matplotlib.pyplot.close(fig)

                # create a plot
                fig, axs = create_plot("Comparison of algorithms with n="+str(
                    n)+f", link density={dens:.2f}" + " and randomized tests=" + str(multi_experiment_size))
                # for each algorithm
                for (datas, name) in [(page_datas, "PageRank"), (hits_datas, "HITS"), (in_datas, "InDegree")]:
                    # plot the data
                    axs[0].plot(xs, combine(statistics.mean, get_prop(
                        datas, "percentage")), label="mean of " + name)
                    axs[1].plot(xs, combine(statistics.mean, get_prop(
                        datas, "ordinal")), label="mean of " + name)
                    axs[2].plot(xs, combine(statistics.mean, get_prop(
                        datas, "biggest")), label="mean of " + name)
                # label, save and close the plot
                lable_plot(fig, axs)
                fig.savefig(PATH+"/out/compare_other/all_"+file +
                            "_"+range_test_description+".jpg")
                matplotlib.pyplot.close(fig)
                # create a plot
                fig, axs = create_plot("Comparison of PageRank and InDegree with n="+str(
                    n)+f", link density={dens:.2f}" + " and randomized tests=" + str(multi_experiment_size))
                # for the algorithms: pagerank and indegree
                for (datas, name) in [(page_datas, "PageRank"), (in_datas, "InDegree")]:
                    # plot the data
                    axs[0].plot(xs, combine(statistics.mean, get_prop(
                        datas, "percentage")), label="mean of " + name)
                    axs[1].plot(xs, combine(statistics.mean, get_prop(
                        datas, "ordinal")), label="mean of " + name)
                    axs[2].plot(xs, combine(statistics.mean, get_prop(
                        datas, "biggest")), label="mean of " + name)
                # label, save and close the plot
                lable_plot(fig, axs)
                fig.savefig(PATH+"/out/compare_other/pageAndIn_" +
                            file+"_"+range_test_description+".jpg")
                matplotlib.pyplot.close(fig)
            # for each algorithm
            for (alg, name) in [("page", "PageRank"), ("hits", "HITS"), ("in", "InDegree")]:
                # create a plot
                fig, axs = create_plot(
                    "Comparison of " + name + " with " + experiment_type)
                # for each test
                for test in data:
                    # get data from json object
                    n = test["n"]
                    dens = 1.0*test["edges"]/(n*n)
                    xs = test["xs"]
                    alg_datas = test[alg]
                    # plot the data
                    axs[0].plot(xs, combine(statistics.mean, get_prop(
                        alg_datas, "percentage")), label="μ with n="+str(n)+" and d="+str(dens))
                    axs[1].plot(xs, combine(statistics.mean, get_prop(
                        alg_datas, "ordinal")), label="μ with n="+str(n)+" and d="+str(dens))
                    axs[2].plot(xs, combine(statistics.mean, get_prop(
                        alg_datas, "biggest")), label="μ with n="+str(n)+" and d="+str(dens))
                # label, save and close the plot
                lable_plot(fig, axs)
                fig.savefig(PATH+"/out/compare_self/all_"+file+"_"+name+".jpg")
                matplotlib.pyplot.close(fig)
    print("Done creating plots")


# program entry point
def main():
    pass
    # running all tests takes approximately 20 hours

    # do 100 tests with 100 samples with graph size 500 for edge denity 2%, 4%, 8%, 16%, 32% adn 64%
    # do_range_test("data_var_dens", [500, 500, 500, 500, 500, 500], [numEdges(500, 0.02), numEdges(500, 0.04), numEdges(500, 0.08), numEdges(500, 0.16), numEdges(500, 0.32), numEdges(500, 0.64)], 100, 100)  # 160M

    # do 100 tests with 100 samples with edge density 20% for the graph sizes 200, 400, 800, 1600 and 3200
    # do_range_test("data_var_size", [200, 400, 800, 1600, 3200], [numEdges(200, 0.2), numEdges(400, 0.2), numEdges(800, 0.2), numEdges(1600, 0.2), numEdges(3200, 0.2)], 100, 100)

    # was used for testing and optimizing speed
    # do_range_test("data_test", [500], [numEdges(500, 0.2)], 10, 10)

    # generate plots and save them as images
    generate_plots()


if __name__ == '__main__':
    main()
